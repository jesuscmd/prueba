import { FETCH_TASKS, FETCH_TASK } from '../actions';

export default function(state = [], action){
	switch(action.type) {
	case FETCH_TASK:
		const task = action.payload;
		const newState = { ...state };
		newState[task.index] = task;
		console.log(state);
		return newState;
		// const task = action.payload.data;
		// const newState = { ...state, };
		// newState[0] = post;
		// return { ...state, [0]: action.payload.data }
		// return action.payload;
	case FETCH_TASKS:
		return action.payload;
	default:
		return state;			
	}
}