import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createTask, fetchTask } from '../actions/';
import TimePicker from 'react-times';

import PageHeader from './page_header';


const data = {
  // used to populate "account" reducer when "Load" is clicked
  task: 'Jane',
  duration: '01:00',
  createdAt: '42:00:00',
}

class TaskAdd extends Component {

	

	constructor(props) {
	  super(props);

    let hour = '';
    let minute = '';

		this.state = {
      hour,
      minute,
      customTime: false,
	  	customTimeValue: "00:01",
	  	isEdition : false,
	  	headerTitle : "Agregar Tarea"
    };
		this.onTimeChange = this.onTimeChange.bind(this);
	  this.handleChange = this.handleChange.bind(this);
	  // console.log(index);
	}

	componentDidMount(){

		const { index } = this.props.match.params;

		if (index) {
			this.props.fetchTask(index);

			this.state.items = this.props.fetchTask(index).payload;

			// this.props.task = this.props.fetchTask(index);

			this.state.isEdition = true;
			this.state.headerTitle = "Editar tarea";
			this.state.title = "dsds";
		}
		console.log(this.props);
		// console.log(this.props.fetchTask(index));
		// this.state.items = this.props.fetchTasks().payload;
		// console.log(this.state.items);
	}




	renderInput(field){
		// console.log(input);
		const { meta : { touched, error } } = field;
		const className = `form-group ${touched && error  ? 'has-danger' : '' }`;
		return (
			<div className={className}>
				<input 
					className="form-control"  
					type="text" 
					placeholder={field.label} 
					// input.value="mono"
					{...field.input} 
				/>
				<small className="form-text text-danger text-center">
					{touched ? error : ''}
				</small>
			</div>
		)
	}

	renderSelect(field){
		const { meta : { touched, error } } = field;
		const className = `form-group ${touched && error  ? 'has-danger' : '' }`;
		return (
				<div className={className}>
					<select
						className="form-control"  
						type="text" 
						{...field.input} 
					>
						<option value="n">Duración de la tarea</option>
					  <option value="00:30">Corta (30min)</option>
					  <option value="00:45">Media (45mins)</option>
					  <option value="01:00">Larga (1h)</option>
					  <option value="Custom">Otra (Hasta 2 h)</option>
					</select>
					
					<small className="form-text text-danger text-center">
						{touched ? error : ''}
					</small>
					
				</div>
		)
	}

	handleChange(event) {
		let customValue =  false;
		if (event.target.value == 'Custom' ) {
			customValue = true;
		}
    this.setState({
      customTime: customValue
    });
  }
	

	onTimeChange(options) {
    const {
      hour,
      minute
    } = options;
    this.setState({ hour, minute});
    let customDuration =  `${options.hour}:${options.minute}`;
    this.state.customTimeValue = customDuration;
  }

	onSubmit(values){

		if (this.state.customTime === true) {
			values.duration = this.state.customTimeValue;
		}

		this.props.createTask(values, () => {
			this.props.history.push('/');
		});
	}

	render (){

		const { handleSubmit } = this.props;
		// console.log(this.props);
		const customTime = this.state.customTime;
		const customTimeValue = this.state.customTimeValue;
		const isEdition = this.state.isEdition;
		const headerTitle = this.state.headerTitle;

		const { task } = this.props;		
		// console.log(isEdition);
		const {
      hour,
      minute
    } = this.state;

		// if (!task) {
		// 	return <div>Cargando...</div>
		// }

		return (
			<div className="task-add wrapper">
				<PageHeader title={headerTitle}/>
		    <div className="section">
		      <div className="container justify-content-center">
		      	<div className="row justify-content-center">
			      	<div className="col-md-6">
				        <form 
				        	onSubmit={handleSubmit(this.onSubmit.bind(this))}
				        	className="justify-content-center"
				        >
				        	<Field
				        		label="Nombre de la tarea"
				        		name="title" 
				        		value={this.state.title}
				        		component={this.renderInput}
				        	/>
				        	<Field
				        		label="Duración"
				        		name="duration" 
				        		component={this.renderSelect}
				        		onChange={this.handleChange}
				        	/>
									
									{customTime == true &&
						        <div>
								      <TimePicker
								      	theme="classic" 
								      	time={customTimeValue ? customTimeValue : "00:01"}
								      	timeConfig={{
											    from: '00:01',
											    to: '02:00',
											    step: 1,
											    unit: 'minutes'
											  }}
									      onTimeChange={this.onTimeChange}
									      {...this.props}
									    />
						        </div>
						      }

				        	<div className="row">
				        		<div className="col-6">
						        	<button type="submit" className="btn btn-primary btn-block">Guardar tarea</button>
						        </div>
						        <div className="col-6">
						        	<Link to="/" className="btn btn-danger btn-block">Cancelar</Link>
						        </div>
				        	</div>
				        </form>
				      </div>
				    </div>
		      </div>
		     </div>
	    </div>
		);
	}

}

function validate(values){
	const errors = {};
	if (!values.title) {
		errors.title="Ingrese un título";
	}
	if (!values.duration || values.duration === 'n') {
		errors.duration="Ingrese una duración";
	}
	return errors;
}

function mapStateToProps({ tasks }, ownProps) {
	return { task: tasks[ownProps.match.params.id]}
}


// function mapStateToProps(state) {
// 	return { tasks: state.tasks };
// }

export default reduxForm({
	validate,
	form: 'PostNewForm'
})(
	connect(mapStateToProps, { createTask, fetchTask })(TaskAdd)
);