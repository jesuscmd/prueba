import _ from 'lodash';
import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchTasks, saveAllTasks} from '../actions';
import { SortableContainer, SortableElement, SortableHandle, arrayMove } from 'react-sortable-hoc';

import PageHeader from './page_header';

const DragHandle = SortableHandle(() => <i className="fas fa-bars mr-3"></i>); // This can be any component you want

const SortableItem = SortableElement(({value}) =>
  <li className="list-group-item">
  	<DragHandle />
  	<span className="task-title">
  		<i className="far fa-clock"></i> <code>{value.duration}</code> | <i className="far fa-square text-muted"></i> {value.title}
  	</span>
  	<button
  		className="btn btn-danger btn-sm float-right m-0" 
  	>
  		<i className="far fa-trash-alt"></i> <span className="d-none d-md-inline">Eliminar</span>
  	</button>
  	<button className="btn btn-primary btn-sm float-right m-0 mr-3">
  		<i className="fas fa-edit"></i> <span className="d-none d-md-inline">Editar</span>
  	</button>
  </li>
)

const SortableList = SortableContainer(({items}) => {
  return (
    <ul className="list-group">
      {items.map((value, index) => (
        <SortableItem key={`item-${index}`} index={index} value={value} />
      ))}
    </ul>
  );
});

class TasksList extends Component {

	componentDidMount(){
		this.props.fetchTasks();
		this.state.items = this.props.fetchTasks().payload || [];
	}


	state = {
    items: []
  };

  onSortEnd = ({oldIndex, newIndex}) => {
    this.setState({
      items: arrayMove(this.state.items, oldIndex, newIndex),
    });
  	this.props.saveAllTasks(this.state.items, () => {
			return true;
		});
  };

	render (){
		return (
			<div className="task-list wrapper">
				
				<PageHeader title="To Do List"/>

		    <div className="section">
		      <div className="container">
		        <div className="button-container">
		          <Link className="btn btn-primary btn-round btn-lg" to="/task/add">
		          	Agregar acción
		          </Link>
		        </div>
		      </div>
		    </div>
		    <div className="section">
		      <div className="container">
		      	<div className="col-12">
		      	<SortableList items={this.state.items} onSortEnd={this.onSortEnd} useDragHandle={true} />
		      	</div>
		      </div>
		    </div>

	    </div>
		);
	}

}

function mapStateToProps(state) {
	return { tasks: state.tasks };
}

export default connect(mapStateToProps, { fetchTasks, saveAllTasks })(TasksList);
