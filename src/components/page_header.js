// import _ from 'lodash';
import React, { Component } from 'react';
// import { connect } from 'react-redux';
// import { fetchTasks } from '../actions';



class PageHeader extends Component {

	render (){
		return (
			<div className="page-header">
				<div className="page-header-image" data-parallax="true">
				</div>
				<div className="content-center">
					<div className="container">
						<h1 className="title">{this.props.title}</h1>
					</div>
				</div>
			</div>
		);
	}

}


export default PageHeader;
