// import _ from 'lodash';
import React, { Component } from 'react';
// import { connect } from 'react-redux';
// import { fetchTasks } from '../actions';



class Navigation extends Component {


	render (){
		return (
			<nav className="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent " color-on-scroll="100">
		    <div className="container">
		      <div className="dropdown button-dropdown">
		        <a href="#pablo" className="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
		          <span className="button-bar"></span>
		          <span className="button-bar"></span>
		          <span className="button-bar"></span>
		        </a>
		        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
		          <a className="dropdown-header">Dropdown header</a>
		          <a className="dropdown-item" href="#">Action</a>
		          <a className="dropdown-item" href="#">Another action</a>
		          <a className="dropdown-item" href="#">Something else here</a>
		          <div className="dropdown-divider"></div>
		          <a className="dropdown-item" href="#">Separated link</a>
		          <div className="dropdown-divider"></div>
		          <a className="dropdown-item" href="#">One more separated link</a>
		        </div>
		      </div>
		      <div className="navbar-translate">
		        <a className="navbar-brand" href="/" rel="tooltip" title="Awesome to do list React App" data-placement="bottom">
		          To Do List
		        </a>
		        <button className="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
		          <span className="navbar-toggler-bar top-bar"></span>
		          <span className="navbar-toggler-bar middle-bar"></span>
		          <span className="navbar-toggler-bar bottom-bar"></span>
		        </button>
		      </div>
		      <div className="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="../assets/img/blurred-image-1.jpg">
		        <ul className="navbar-nav">
		          <li className="nav-item">
		            <a className="nav-link" href="/historic">Historic</a>
		          </li>
		          <li className="nav-item">
		            <a className="nav-link" href="/graph">Graph</a>
		          </li>
		          <li className="nav-item">
		            <a className="nav-link" rel="tooltip" title="Follow me on Behance" data-placement="bottom" href="https://www.behance.net/jcmd" target="_blank">
		              <i className="fab fa-behance"></i>
		              <p className="d-lg-none d-xl-none">Behance</p>
		            </a>
		          </li>
		
		          <li className="nav-item">
		            <a className="nav-link" rel="tooltip" title="Follow me on Instagram" data-placement="bottom" href="https://www.instagram.com/jesuscmd" target="_blank">
		              <i className="fab fa-instagram"></i>
		              <p className="d-lg-none d-xl-none">Instagram</p>
		            </a>
		          </li>
		        </ul>
		      </div>
		    </div>
		  </nav>

		);
	}

}


export default Navigation;
