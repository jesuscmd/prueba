export const FETCH_TASKS = 'fetch_tasks';
export const FETCH_TASK = 'fetch_task';
export const CREATE_TASK = 'create_task';
export const SAVE_ALLTASKS = 'save_alltasks';

export function fetchTasks() {
	// GET TASK FROM LOCAL STORAGE with a PROMISE
	const request = JSON.parse(localStorage.getItem('tasks'));
	// console.log(request);
	return {
		type: FETCH_TASKS,
		payload: request
	};
}

export function createTask(value, callback) {

	let existingEntries = JSON.parse(localStorage.getItem("tasks"));

  if(existingEntries == null) existingEntries = [];

	value.creation = new Date;
  existingEntries.unshift(value);
  localStorage.setItem("tasks", JSON.stringify(existingEntries));
  callback();

	return {
		type: CREATE_TASK,
		payload: existingEntries
	};
}

export function saveAllTasks(value, callback) {

  localStorage.setItem("tasks", JSON.stringify(value));
  callback();
	return {
		type: SAVE_ALLTASKS,
		payload: true
	};
}

export function fetchTask(index) {
	let currentTask = null;
	if (index) {
	  let tasks =  JSON.parse(localStorage.getItem("tasks"));
	  currentTask = tasks[index - 1];
	  currentTask.index = index;
	}
	  // console.log(currentTask);
  // callback();
	return {
		type: FETCH_TASK,
		payload: currentTask
	};
}
