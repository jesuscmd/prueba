import React from 'react';
import ReactDOM , { render } from 'react-dom';
import { Provider } from 'react-redux';
// Import Redux
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
// import { SortableContainer, SortableElement, arrayMove } from 'react-sortable-hoc';

import promise from 'redux-promise';
import reducers from './reducers';
import TasksList from './components/tasks_list';
import Navigation from './components/navigation';
import TaskHistoric from './components/task_history';
import TaskAdd from './components/task_add';

// import { PageHeader } from './components/page_header';


// import AddTask from './components/add_task';


const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

// Configure routes
ReactDOM.render(
	<Provider store={createStoreWithMiddleware(reducers)}>
		<BrowserRouter>
			<div>
				<Navigation></Navigation>
				<Switch>
					<Route path="/historic" component={TaskHistoric} />
					<Route path="/task/add" component={TaskAdd} />
					<Route path="/task/:index" component={TaskAdd} />
					<Route path="/" component={TasksList} />
				</Switch>
			</div>
		</BrowserRouter>
	</Provider>
	, document.querySelector('.main-wrapper'));






				// <Route path="/graph" component={ProductivityGraph} />
